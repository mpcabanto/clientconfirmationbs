const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();
const https = require('https');
const fs = require('fs');
const port = 3000;
const hostname = "localhost";
const cacheControl = require('express-cache-controller');


var key = fs.readFileSync(__dirname + '/certs/selfsigned.key');
var cert = fs.readFileSync(__dirname + '/certs/selfsigned.crt');
var options = {
    key: key,
    cert: cert
};

app.disable('x-powered-by');
app.use(cacheControl({
    private: true,
    noCache: true,
    noStore: true,
    mustRevalidate: true,    
    }))
app.use(express.static(path.join(__dirname,'build')));
app.use(express.static('private', {
etag: false,
lastModified: true,
setHeaders: (res, path) => {
    if (path.endsWith('.html')) {
        res.setHeader('Cache-Control','no-cache');
            }
        }
      }
    ));

    app.use((req, res, next) => {
        res.setHeader('Access-Control-Allow-Origin', 'https://10.11.27.104');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, HEAD');
        res.setHeader(
          'Access-Control-Allow-Headers',
          'Origin, X-Requested-With, Content-Type, Accept, Authorization'
        );
        res.setHeader('Expires', new Date(Date.now() + 2592000000  ).toUTCString ) ;
            res.setHeader('Cache-Control','no-cache'  );
        next();
      });
      

app.get('/*', function(req, res, next)
{
 //   res.setHeader('Expires', new Date(Date.now() + 2592000000  ).toUTCString ) ;
 //   res.setHeader('Cache-Control','private, max-age=2592000'  );
            res.setHeader('Cache-Control','no-cache'  );
    res.sendFile(path.join(__dirname, 'build', 'index.html'));
}

);

var server = https.createServer(options, app);

server.listen( port, () => {
    console.log('Frontend start on port: ' + port )
});