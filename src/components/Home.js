import React, { Component, Fragment } from 'react';
import Secured from './Secured';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { Pane, Text, Heading, TextInput, Select, SelectField, RadioGroup, Combobox, Button } from 'evergreen-ui';

export class Home extends Component {
  displayName = Home.name

  constructor(props) {
    super(props);
    this.state = {
      referenceNumber: ""
    }
  }


  fieldOnChange = sender => {
    let value = sender.target.value;
    this.setState({referenceNumber: value });
  }

  render() {
    return (
        <div >
            <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3}  height={100} >
          <Pane display="flex" width="100%"  alignItems="center" justifyContent="center">
            <Heading size={600} marginLeft={20} color="#1070CA" >PSBANK - Onboarding Client Confirmation</Heading>
          </Pane>
        </Pane>
<Pane display="flex"  background="tint2" borderRadius={3} height={100}>
  <Pane display="flex" width="100%" marginLeft={12} alignItems="center" justifyContent="center">
    <Pane width="50%"
      marginRight={10}  alignItems="center" justifyContent="center">

      <Heading size={400}  >Please input Reference Number:</Heading>
      <br />
      <TextInput
                name="refno"
                value={this.state.referenceNumber}
                placeholder="Reference Number"
                width="100%"
                required
                onChange={this.fieldOnChange}
              >
              </TextInput>
    </Pane>
  </Pane>
</Pane>

                <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" alignItems="center" justifyContent="center"> <Link to={{ pathname: "/ClientConfirmation1", state: this.state.referenceNumber }}>
            <Button height={36} marginRight={16} appearance="primary" >Start</Button> </Link>
          </Pane>
        </Pane>
            </div>
    );
  }
}
