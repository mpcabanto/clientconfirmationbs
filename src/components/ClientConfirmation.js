import React, { Component } from 'react';
import Panel from 'react-bootstrap/lib/Panel'
import axios from 'axios';
import { Pane, Text,Icon, Heading, TextInput, Table, Select, SelectField, RadioGroup, Combobox, Button, Switch, TextInputField, Spinner } from 'evergreen-ui';
import { Link, Redirect } from 'react-router-dom';
import 'antd/dist/antd.css';
import { Radio, DatePicker } from 'antd';
import moment from 'moment';
import styles from './styles.css';
import CameraModal from './CameraModal';
import ButtonModal from './Clientmodal.js';
import Draggable, { DraggableCore } from 'react-draggable';
import SweetAlert from 'react-bootstrap-sweetalert';

import SignaturePad from 'react-signature-pad-wrapper';
import Grid from '@material-ui/core/Grid';
import CardMedia from '@material-ui/core/CardMedia';
import { myConfig } from '../config.js';

//This Component is a child Component of Customers Component
export class ClientConfirmation extends Component {
  displayName = ClientConfirmation.name

  constructor(props) {
    super(props);
    this.loadProduct = this.loadProduct.bind(this)
    this.state = {
      options: [
        { label: 'Yes', value: 'Y' },
        { label: 'No', value: 'N' }
      ],
      listDocType: [
        { label: "SSS UMID", value: "0005" },
        { label: "Driver's License", value: "0003" },
        { label: "Passport", value: "0006" },
        { label: "TIN", value: "0004" }
      ],
      CapturedIDList: [],
      vFatka: 'N',
      vConfirmadd: 'N',
      referenceNumber: null,
      vmunicipality: '', municipality: '', municipalityTempList: [], municipalityTemp2List: [], municipalityTemp3List: [],
      tempval: '',
      vmailaddress: 'PERM',

      trimmedDataURL: null,
      trimmedDataURLClientPhoto: null,
      trimmedDataURLClientID: null,
      signatureblob: null,
      convertedImg: 'x',
      ImageSuccessfullyUploaded: "F",
      convertedImgClientPhoto: 'x',
      convertedImgClientID: 'x',

      vloading: true,
      PopUpState: null,

      presentAddress: {},
      permanentAddress: {},
      vDocType: "1",
      fields: {
        firstname: '',
        middlename: '',
        lastname: '',
        birthdate: '',
        mobilenumber: '',
        nationality: 'FILIPINO',
        placeofbirth: 'PH-CAL',
        email: '',
        unitnopres: '',
        bldgpres: '',
        unitnoperm: '',
        bldgperm: '',
        cif: '00000000',
        userid: 'CLIENT',
        customertype: '',
        gender: 'M',
        civilstatus: 'M',
        spousename: 'NOT APPLICABLE',
        employername: 'EMPLOYER NAME',
        employeraddress: 'EMPLOYER ADDRESS',
        monthlyincome: "0",
        documenttype: '',
        producttype: '',
        rccode: '111',
        accountfacilities: '',
        fatcadeclaration: '',
        applicationrefno: '',
        streetpres: "",
        barangaypres: "n/a",
        citypres: "2800",
        provincepres: "PH-CAL",
        countrypres: "AFG",
        zipcodepres: "",
        streetperm: "",
        barangayperm: "n/a",
        cityperm: "2800",
        provinceperm: "PH-CAL",
        countryperm: "AFG",
        zipcodeperm: "",
        provincemail: "PH-CAL",
        countrymail: "AFG",
        zipcodemail: "",
        sourceincome: "0001",
        naturework: "0001"
      }
    }

    this.fieldProvinceChange = this.fieldProvinceChange.bind(this)
  }

  //SigPad
  handleClear() {
    this.signaturePad.instance.clear();
  }

  dataURItoBlob(dataURI) {
    let byteString = atob(dataURI.split(',')[1]);
    // separate out the mime component
    let mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
    let ab = new ArrayBuffer(byteString.length);
    let ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    let blob = new Blob([ab], { type: mimeString });
    return blob;
  }

  convertBlob(blob) {
    let fileReaderInstance = new FileReader();
    fileReaderInstance.readAsDataURL(blob);
    fileReaderInstance.onload = () => {
      let base64data = fileReaderInstance.result;
      this.setState({ convertedImg: base64data });
    }
  }

  handleSave() {
    console.log(this.signaturePad.isEmpty())
    if (this.signaturePad.isEmpty()) {
      alert('Please provide a signature first.');
    } else {
      sessionStorage.setItem("ClientSig", this.signaturePad.toDataURL())  //naglagay ka sa session storage client sig ng image
      this.setState({ trimmedDataURL: sessionStorage.getItem("ClientSig") }) //from session storage nilagay sa trimmeddataurl
      let blob = this.dataURItoBlob(sessionStorage.getItem("ClientSig")); //from session storage nilagay sa blob
      let fileReaderInstance = new FileReader();
      fileReaderInstance.readAsDataURL(blob);
      fileReaderInstance.onload = () => {
        let base64data = fileReaderInstance.result;
        this.setState({ convertedImg: fileReaderInstance.result.substring(22) });  //nilagay sa convertedimg yung parsed na tinanggal yung extra strings
      }
    }
  }
  //end of SigPad

  callbackFunction = (childData) => {
    console.log(childData.DocumentDescription);
    console.log(childData.DocumentCode);
    if (childData.DocumentCode === "0059") {
      this.setState({
        trimmedDataURLClientPhoto: childData.DocumentImage
      });
    }
    else {
      if (childData.DocumentDescription.trim() !="")
      {  
        this.setState(prevState => ({ CapturedIDList: [...prevState.CapturedIDList, { DocumentCode: childData.DocumentCode, DocumentImage: childData.DocumentImage, DocumentImageFileExt: childData.DocumentImageFileExt, DocumentDescription: childData.DocumentDescription }] }))   }
    }
    console.log(this.state.CapturedIDList);
  }






  //for alert
  PopTheMessage(param, sweettype) {
    const getAlert = () =>
      (
        <SweetAlert sweettype onConfirm={() => this.hideAlert()}>{param}</SweetAlert>
      );
    this.setState({ LearnMore: getAlert() });
  }
  hideAlert() {
    console.log('Hiding alert...');
    this.setState({ LearnMore: null });
    console.log(this.state)
  }
  //for alert


  fieldOnChange = sender => {
    let fieldName = sender.target.name;
    let value = sender.target.value;
    let state = this.state;
    this.setState({
      ...state,
      fields: { ...state.fields, [fieldName]: value }
      ,
    });
  }

  ChangeIDType = sender => {
    let fieldName = sender.target.name;
    let value = sender.target.value;
    this.setState({ vDocType: value });
    console.log(value);
  }


  fieldProvinceChange = sender => {
    let fieldName = sender.target.name;
    let value = sender.target.value;
    let state = this.state;
    let vselectedprov = value.substring(0, 6)

    this.setState({
      ...state,
      fields: { ...state.fields, [fieldName]: value, "countrypres": "PH" }
    });
    const config = {
      mode: "no-cors",
      headers: {
        "Access-Control-Allow-Origin": "GET, POST",
        "Access-Control-Max-Age": "86400",
        "Access-Control-Allow-Headers": "Content-Type",
        "Content-Type": "application/x-www-form-urlencoded",
      }
    }
    axios.all([
      axios.post(myConfig.apimiddlewareurl, { APIurl: myConfig.apiuri_getmunicipality, APIdata: vselectedprov, APImethod: "GET" }, { config })
    ])
      .then(axios.spread((data2) => {
        this.setState({
          municipalityTempList: data2.data,
        })
      }
      ))
  }

  fieldProvinceChange2 = sender => {
    let fieldName = sender.target.name;
    let value = sender.target.value;
    let state = this.state;
    let vselectedprov = value.substring(0, 6)

    this.setState({
      ...state,
      fields: { ...state.fields, [fieldName]: value, "countryperm": "PH" }
    });
    const config = {
      mode: "no-cors",
      headers: {
        "Access-Control-Allow-Origin": "GET, POST",
        "Access-Control-Max-Age": "86400",
        "Access-Control-Allow-Headers": "Content-Type",
        "Content-Type": "application/x-www-form-urlencoded",
      }
    }
    axios.all([
      axios.post(myConfig.apimiddlewareurl, { APIurl: myConfig.apiuri_getmunicipality, APIdata: vselectedprov, APImethod: "GET" }, { config })
    ])
      .then(axios.spread((data2) => {
        this.setState({
          municipalityTemp2List: data2.data,
        })
      }
      ))
    this.setState({
      countryperm: "PH"
    });
  }

  fieldProvinceChange3 = sender => {
    let fieldName = sender.target.name;
    let value = sender.target.value;
    let state = this.state;
    let vselectedprov = value.substring(0, 6)
    this.setState({
      ...state,
      fields: { ...state.fields, [fieldName]: value, "countrymail": "PH" }
    });
    const config = {
      mode: "no-cors",
      headers: {
        "Access-Control-Allow-Origin": "GET, POST",
        "Access-Control-Max-Age": "86400",
        "Access-Control-Allow-Headers": "Content-Type",
        "Content-Type": "application/x-www-form-urlencoded",
      }
    }
    axios.all([
      axios.post(myConfig.apimiddlewareurl, { APIurl: myConfig.apiuri_getmunicipality, APIdata: vselectedprov, APImethod: "GET" }, { config })
    ])
      .then(axios.spread((data3) => {
        this.setState({
          municipalityTemp3List: data3.data,
        })
      }
      ))
    this.setState({
      countrymail: "PH"
    });
  }

  MunicipalityChange = sender => {
    let value = sender.target.value;
    let state = this.state;
    this.setState({
      fields: {
        ...state.fields, "citypres": value
        , "zipcodepres": value, "countrypres": "PH"
      }
    });
  }

  MunicipalityChange2 = sender => {
    let value = sender.target.value;
    let state = this.state;
    this.setState({
      fields: {
        ...state.fields, "cityperm": value
        , "zipcodeperm": value
      }
    });
  }

  MunicipalityChange3 = sender => {
    let value = sender.target.value;
    let state = this.state;
    this.setState({
      fields: {
        ...state.fields, "citymail": value
        , "zipcodemail": value
      }
    });
  }

  birthdateChange = date => {
    let state = this.state;
    let value = moment(date).format('MM/DD/YYYY');
    this.setState({
      ...state,
      fields: { ...state.fields, "birthdate": value }
    });
  }

  filesOnChange = sender => {
    let files = sender.target.files;
    let state = this.state;
    this.setState({
      ...state,
      files: files
    });
  }

  onAddressChange = e => {
    if (e.target.value == "Y") {
      const config = {
        mode: "no-cors",
        headers: {
          "Access-Control-Allow-Origin": "GET, POST",
          "Access-Control-Max-Age": "86400",
          "Access-Control-Allow-Headers": "Content-Type",
          "Content-Type": "application/x-www-form-urlencoded",
        }
      }
      axios.all([
        axios.post(myConfig.apimiddlewareurl, { APIurl: myConfig.apiuri_getmunicipality, APIdata: this.state.fields.provincepres.substring(0, 6), APImethod: "GET" }, { config })
      ])
        .then(axios.spread((data2) => {
          this.setState({
            municipalityTemp2List: data2.data,
          })
        }
        ))
      let state = this.state;
      this.setState({
        fields: {
          ...state.fields, "streetperm": state.fields.streetpres, "cityperm": state.fields.citypres
          , "provinceperm": state.fields.provincepres, "countryperm": state.fields.countrypres, "zipcodeperm": state.fields.zipcodepres
        }
      });
    } else {
      this.setState({ vConfirmadd: "N" });
    }
  };

  loadProduct = event => {
    if (this.state.fields.firstname.trim() != "" && this.state.fields.middlename.trim() != "" && this.state.fields.lastname.trim() != ""
      && this.state.fields.birthdate.trim() != "" && this.state.fields.mobilenumber.trim() != "" && this.state.fields.email.trim() != ""
      && this.state.fields.streetpres.trim() != "" && this.state.fields.zipcodepres.trim() && this.state.fields.streetperm.trim() != "" && this.state.fields.zipcodeperm.trim()
    ) { this.props.history.push({ pathname: '/productinfo', state: this.state.fields }) }
    else { alert("Error in confirming client details."); }
  }

  selectCountry(val) {
    this.setState({ country: val });
  }

  selectMunicipality(val) {
    this.setState({ municipality: val });
  }

  selectProvince(val) {
    this.setState({ province: val });
  }

  selectRegion(val) {
    this.setState({ region: val });
  }

  onAddressChange = e => {
    if (e.target.value == "Y") {
      const config = {
        mode: "no-cors",
        headers: {
          "Access-Control-Allow-Origin": "GET, POST",
          "Access-Control-Max-Age": "86400",
          "Access-Control-Allow-Headers": "Content-Type",
          "Content-Type": "application/x-www-form-urlencoded",
        }
      }
      axios.all([
        axios.post(myConfig.apimiddlewareurl, { APIurl: myConfig.apiuri_getmunicipality, APIdata: this.state.fields.provincepres.substring(0, 6), APImethod: "GET" }, { config })
      ])
        .then(axios.spread((data2) => {
          this.setState({
            municipalityTemp2List: data2.data,
          })
        }
        ))

      let state = this.state;
      this.setState({
        fields: {
          ...state.fields, "streetperm": state.fields.streetpres, "cityperm": state.fields.citypres
          , "provinceperm": state.fields.provincepres, "countryperm": state.fields.countrypres, "zipcodeperm": state.fields.zipcodepres
        }
      });
    } else {
      this.setState({ vConfirmadd: "N" });
    }
  };


  //to display clientpho
  ReturnClientPhoto() {
    console.log(JSON.parse(sessionStorage.getItem("clientPhoto")));
    if (JSON.parse(sessionStorage.getItem("clientPhoto")) !== null) {//this.state.trimmedDataURLClientPhoto}
      return JSON.parse(sessionStorage.getItem("clientPhoto")).DocumentImage
    }
  }
  //to display clientpho



  //for alert
  PopTheMessage(x) {
    const getAlert = () =>
      (
        <SweetAlert onConfirm={() => this.hideAlert()}>{x} </SweetAlert>
      );
    this.setState({ LearnMore: getAlert() });
  }
  hideAlert() {
    console.log('Hiding alert...');
    this.setState({ LearnMore: null });
    console.log(this.state)
  }
  //for alert  

  onMailAddressTagChange = e => {
    let value = e.target.value;
    this.setState({ vmailaddress: e.target.value });
  }

  renderMailAddress() {
    const { classes } = this.props;
    const { clientInfo } = this.state;
    if (this.state.fields.streetmail != "") {
      return (
        <div>
          {/* 10th Line */}
          <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
            <Pane display="flex" width="100%" marginLeft={12}>
              <Pane width="81%"
                marginRight={10}>
                <TextInput
                  name="streetmail"
                  value={this.state.fields.mailingAddress.address}
                  placeholder="Mailing Address (House / Street / Barangay)"
                  width="100%"
                  onChange={this.fieldOnChange}
                >
                </TextInput>
              </Pane>
            </Pane>
          </Pane>

          {/* 11th Line */}
          <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
            <Pane display="flex" width="100%" marginLeft={12}>
              <Pane width="20%"
                marginRight={10}>

                <SelectField
                  name="provincemail"
                  disabled
                  value={this.state.fields.mailingAddress.province}
                  width="100%"
                  onChange={this.fieldProvinceChange3.bind(this)}
                  description="Province"
                >
                  {this.state.provinceList.data.map((vprovince) => {
                    return <option value={vprovince.code}> {vprovince.name} </option>
                  })}
                </SelectField>

              </Pane>
              <Pane width="20%" marginRight={10}>
                <SelectField
                  name="citymail"
                  value={this.state.fields.mailingAddress.city}
                  width="100%"
                  onChange={this.MunicipalityChange3}
                  description="City/Municipality"
                  disabled
                >
                  {this.state.municipalityTemp3List.data.map((vmuni3) => {
                    return <option value={vmuni3.code}> {vmuni3.name} </option>
                  })}
                </SelectField>
              </Pane>

              <Pane width="20%"
                marginRight={10}>
                <SelectField
                  name="countrymail"
                  value={this.state.fields.mailingAddress.country}
                  width="100%"
                  onChange={this.fieldOnChange}
                  description="Country"
                  disabled
                >
                  {this.state.countriesList.data.map((vcountries) => {
                    return <option value={vcountries.alpha2} > {vcountries.name} </option>
                  })}
                </SelectField>
              </Pane>
              <Pane width="18%"
                marginRight={10}>
                <Text size={301} marginLeft={1} >Zip Code</Text>
                <TextInput
                  name="zipcodemail"
                  value={this.state.fields.mailingAddress.zipCode}
                  placeholder="Zip Code"
                  width="100%"
                  onChange={this.fieldOnChange}
                >
                </TextInput>
              </Pane>
            </Pane>
          </Pane>
        </div>

      );
    }
    else {
    }
  }













  //Function which is called when the component loads for the first time
  componentDidMount() {

    console.log(this.state.fields.nationality);
    if (!this.props.location.state) {
      alert("No Reference Number.");
      this.props.history.push("/")
    } else {
      this.setState({ referenceNumber: this.props.location.state });

      //getting the customer records
      try { this.getCustomerDetails(); }
      catch (e) { this.PopTheMessage("SAVINGS1") } { this.state.PopUpState };
      //getting the customer records
    }
  }
  getCustomerDetails = async () => {
    const headers = { "headers": "Access-Control-Allow-Origin" }
    let rc = localStorage.getItem("groups");
    let vappno = this.state.vRefNo;
    axios.post(myConfig.apimiddlewareurl, { APIurl: myConfig.apiuri_getcustomerforconfirmation, APIdata: "referenceNumber=" + this.props.location.state, APImethod: "GET" })
      .then(response => {
        let results = null;
        if (typeof (response.data) === "string") {
          results = JSON.parse(response.data);
        } else {
          results = response.data;
        }
        this.setState({ fields: results.data }, () => { this.getAPI(); });
        this.setState({ vloading: false });
      })
      .catch((ex) => {
        console.error(ex);
        this.setState({ isError: true });
        alert("Reference number doesn't exists");
        this.props.history.push({ pathname: '/', state: this.state.fields });
      });

  }; //end getCustomerDetails

  getAPI = async () => {
    const headers = { "headers": "Access-Control-Allow-Origin" }
    await axios.all([
      axios.post(myConfig.apimiddlewareurl, { APIurl: myConfig.apiuri_getcountry, APIdata: '', APImethod: "GET" }),
      axios.post(myConfig.apimiddlewareurl, { APIurl: myConfig.apiuri_getmunicipality, APIdata: this.state.fields.presentAddress.province, APImethod: "GET" }),
      axios.post(myConfig.apimiddlewareurl, { APIurl: myConfig.apiuri_getprovince, APIdata: '', APImethod: "GET" }),
      axios.post(myConfig.apimiddlewareurl, { APIurl: myConfig.apiuri_getincomesource, APIdata: '', APImethod: "GET" }),
      axios.post(myConfig.apimiddlewareurl, { APIurl: myConfig.apiuri_getprofession, APIdata: '', APImethod: "GET" }),
      axios.post(myConfig.apimiddlewareurl, { APIurl: myConfig.apiuri_getmunicipality, APIdata: this.state.fields.permanentAddress.province, APImethod: "GET" }),
      axios.post(myConfig.apimiddlewareurl, { APIurl: myConfig.apiuri_getmunicipality, APIdata: this.state.fields.mailingAddress.province, APImethod: "GET" })
    ])
      .then(axios.spread((data1, data2, data3, data4, data5, data6, data7) => {
        this.setState({
          countriesList: data1.data,
          countrypres: "PH",
          municipalityList: data2.data, municipalityTempList: data2.data, municipalityTemp2List: data6.data, municipalityTemp3List: data7.data,
          provinceList: data3.data,
          incomesourceList: data4.data,
          professionList: data5.data
        })
      }
      ))
    let state = this.state;
    this.setState({
      ...state,
      fields: { ...state.fields, "countrypres": "PH", "countryperm": "PH", "countrymail": "PH" }
    });
  }

  //magdisplay pag inupload
  uploadImage = (value, event) => {
    this.setState({ vloading: true });
    try {
      this.setState({
        convertedImgClientPhoto: JSON.parse(sessionStorage.getItem("clientPhoto")).DocumentImage.substring(23),
        convertedImgClientID: JSON.parse(sessionStorage.getItem("clientID")).DocumentImage.substring(23),
        trimmedDataURLClientID: sessionStorage.getItem("clientID").toDataURL,
        trimmedDataURLClientPhoto: sessionStorage.getItem("clientPhoto")
      });
      this.setState({ trimmedDataURL: sessionStorage.getItem("ClientSig") })
    }
    catch (e) {
      console.log(e);
    }

    if (this.state.convertedImg != 'x') {
      const config = {
        mode: "no-cors",
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/x-www-form-urlencoded",
          "Accept": "application/json"
        }
      };
      const formD =
        {
          file: this.state.convertedImg,
          isFront: 1,
          referenceNumber: this.props.location.state,
          level: 3,
          documentType: 2,
          requestor: "CLIENT",
          fileExtension: "jpg"
        };

      axios.post(myConfig.apimiddlewareurl, { APIurl: myConfig.apiuri_uploaddocument, APIdata: formD, APImethod: "POST" })
        .then(response => {
          this.setState({ createaccountresponse: response.data });
          this.setState({ submittedjson: formD });
          // console.log(form1);
          if (this.state.createaccountresponse.errors) {
            alert(JSON.stringify(this.state.createaccountresponse.errors, null, 4));
          } else {
            this.setState({ ImageSuccessfullyUploaded: "T" });
            this.uploadConfirmation();
            this.uploadImageClientPhoto();
            this.uploadImageClientID();
            this.setState({ vloading: false });
          }
        })
        .catch((ex) => {
          console.error(ex);
          this.setState({ isError: true });
          alert(JSON.stringify(this.state.createaccountresponse.errors, null, 4));
        });

    } else { this.uploadConfirmation(); }
    this.setState({ vloading: false });
  }//end uplodForm

  uploadConfirmation = (value, event) => {
    this.setState({ ProductType: { value } });
    const config = {
      mode: "no-cors",
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/x-www-form-urlencoded",
        "Accept": "application/json"
      }
    };

    const form1 =
      {
        dedupDisposition: 0,
        riskProfile: "",
        referenceNumber: this.props.location.state,
        processType: 0,
        riskScore: 0,
        createdBy: "CLIENT",
        riskRating: "",
        remarks: "Client details confirmed by client",
        status: 20,
        statusDate: moment().format("MM/DD/YYYY"), ///date.getmonth()+1 + "/" + date.getDate() + "/" +  date.gettyear(),//Date.now.format("mm/dd/yyyy")"",
        icdbResult: 0,
        appLevel: 3,
        statusDescription: "CLIENT CONFIRMATION"
      };

    axios.post(myConfig.apimiddlewareurl, { APIurl: myConfig.apiuri_statusupdate, APIdata: form1, APImethod: "POST" })
      .then(response => {
        this.setState({ createaccountresponse: response.data });
        this.setState({ submittedjson: form1 });
        console.log(this.state.createaccountresponse.errors);
        if (this.state.createaccountresponse.errors) {
          alert(JSON.stringify(this.state.createaccountresponse.errors, null, 4));
        } else {
          alert("Your personal details has been confirmed. Thank you.");
          this.props.history.push({ pathname: '/', state: this.state.fields });
        }
      })
      .catch((ex) => {
        console.error(ex);
        this.setState({ isError: true });
        alert(JSON.stringify(this.state.createaccountresponse.errors, null, 4));
      });
  }//end uplodForm




  uploadImageClientPhoto = (value, event) => {
    if (this.state.convertedImgClientPhoto != 'x' && this.state.convertedImgClientPhoto != '') {
      const config = {
        mode: "no-cors",
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/x-www-form-urlencoded",
          "Accept": "application/json"
        }
      };

      const formF =
        {
          file: this.state.convertedImgClientPhoto,
          isFront: 1,
          referenceNumber: this.props.location.state,
          level: 3,
          documentType: 1,
          requestor: "CLIENT",
          fileExtension: "jpg"
        };

      axios.post(myConfig.apimiddlewareurl, { APIurl: myConfig.apiuri_uploaddocument, APIdata: formF, APImethod: "POST" })
        .then(response => {
          this.setState({ createaccountresponse: response.data });
          this.setState({ submittedjson: formF });
          console.log(this.state.createaccountresponse.errors);
          if (this.state.createaccountresponse.errors) {
            alert(JSON.stringify(this.state.createaccountresponse.errors, null, 4));
          } else {
            this.setState({ ImageSuccessfullyUploaded: "T" });
          }
        })
        .catch((ex) => {
          console.error(ex);
          this.setState({ isError: true });
          alert(JSON.stringify(this.state.createaccountresponse.errors, null, 4));
        });

    }
  }//end uplodForm

  uploadImageClientID = (value, event) => {
    if (this.state.convertedImgClientID != 'x' && this.state.convertedImgClientID != '') {
      const config = {
        mode: "no-cors",
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/x-www-form-urlencoded",
          "Accept": "application/json"
        }
      };

      const formE =
        {
          file: this.state.convertedImgClientID,
          isFront: 1,
          referenceNumber: this.props.location.state,
          level: 3,
          documentType: this.state.fields.vDocType,
          requestor: "CLIENT",
          fileExtension: "jpg"
        };

      axios.post(myConfig.apimiddlewareurl, { APIurl: myConfig.apiuri_uploaddocument, APIdata: formE, APImethod: "POST" })
        .then(response => {
          this.setState({ createaccountresponse: response.data });
          this.setState({ submittedjson: formE });
          console.log(this.state.createaccountresponse.errors);
          if (this.state.createaccountresponse.errors) {
            alert(JSON.stringify(this.state.createaccountresponse.errors, null, 4));
          } else {
            this.setState({ ImageSuccessfullyUploaded: "T" });
          }
        })
        .catch((ex) => {
          console.error(ex);
          this.setState({ isError: true });
          alert(JSON.stringify(this.state.createaccountresponse.errors, null, 4));
        });

    }
  }//end uplodForm


  renderTableData() {
    return this.state.CapturedIDList.map((IDs, index) => {
      const { DocumentCode, DocumentDescription } = IDs
      return (
        <tr key={DocumentCode}>
          <td width="85%"><Pane>{DocumentDescription}</Pane> </td>
          <td width="15%"><Pane> <Icon icon="trash" color="sucess" marginRight={16} /></Pane> </td>
        </tr>
      )
    })
  }




  render() {


    //  let gago= JSON.parse(sessionStorage.getItem("clientPhoto")).DocumentImage
    //municipalityTempList

    const { options, vFatka, vConfirmadd, values, presentAddress, permanentAddress } = this.state;
    console.log(sessionStorage.getItem("ClientSig"));
    const { province, municipality, country, region } = this.state;
    const { trimmedDataURL } = this.state
    const { trimmedDataURLClientID } = this.state
    const { trimmedDataURLClientPhoto } = this.state


    if (!this.state.countriesList || this.state.vloading)
      return (
        <Pane display="flex" alignItems="center" justifyContent="center" height={400}>
          {this.state.vloading ? <Spinner /> : ""}
        </Pane>
      )
    return (
      <div >
        {/* 1st Line */}
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="100%" >
            <Heading size={600} marginLeft={12} >Personal Information</Heading>
          </Pane>
        </Pane>

        {/* 2nd Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="30%"
              marginRight={10}>
              <TextInput
                name="firstname"
                placeholder="First Name"
                width="100%"
                required
                validationMessage="This field is required"
                value={this.state.fields.firstName}
              >
              </TextInput>
            </Pane>
            <Pane width="30%"
              marginRight={10}>
              <TextInput
                name="middlename"
                placeholder="Middle Name"
                width="100%"
                required
                validationMessage="This field is required"
                value={this.state.fields.middleName}
              >
              </TextInput>
            </Pane>
            <Pane width="30%"
              marginRight={10}>
              <TextInput
                name="lastname"
                placeholder="Last Name, Suffix"
                width="100%"
                required
                validationMessage="This field is required"
                value={this.state.fields.lastName}
              >
              </TextInput>
            </Pane>
          </Pane>
        </Pane>

        {/* 3rd Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="30%"
              marginRight={10}>
              <Text size={301} marginLeft={1} >Nationality</Text>
              <SelectField disabled
                name="nationality"
                value={this.state.fields.nationality}
                width="100%"
                onChange={this.fieldOnChange}
              > {this.state.countriesList.data.map((vcountries) => {
                return <option value={vcountries.alpha2} > {vcountries.name} </option>
              })}
              </SelectField>
            </Pane>
            <Pane width="30%"
              marginRight={10}>
              <Text size={301} marginLeft={1} >Birth Date           </Text>
              {/*<Text marginRight={15}>BirthDate:</Text> */}
              <DatePicker format='MM/DD/YYYY' disabled
                value={moment(this.state.fields.birthDate)}
              >
              </DatePicker>
            </Pane>

            <Pane width="30%"
              marginRight={10}>
              <SelectField
                name="placeofbirth"
                disabled
                value={this.state.fields.birthPlace}
                width="100%"
                description="Place of Birth"
              >
                {this.state.provinceList.data.map((vprovince) => {
                  return <option value={vprovince.code}    > {vprovince.name} </option>
                })}
              </SelectField>
            </Pane>
          </Pane>
        </Pane>

        {/* 4th Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="50%"
              marginRight={10}>
              <Text marginRight={15}>US Person:</Text>
              <Radio.Group defaultValue="N" disabled
                name="fatcadeclaration">
                <Radio value="Y">Yes</Radio> <Radio value="N">No</Radio>
              </Radio.Group>
            </Pane>
          </Pane>
        </Pane>

        {/* 5th Line */}
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="100%" >
            <Heading size={600} marginLeft={12} >Contact Information</Heading>
          </Pane>
        </Pane>

        {/* 6th Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="40%"
              marginRight={10}>
              <TextInput
                name="mobilenumber"
                placeholder="Mobile Number"
                width="100%"
                required
                validationMessage="This field is required"
                value={this.state.fields.mobileNumber}
              // onChange={this.fieldOnChange}
              >
              </TextInput>
            </Pane>
            <Pane width="40%"
              marginRight={10}>
              <TextInput
                name="email"
                placeholder="E-mail"
                width="100%"
                required
                validationMessage="This field is required"
                value={this.state.fields.emailAddress}
              //       onChange={this.fieldOnChange}
              >
              </TextInput>
            </Pane>
          </Pane>
        </Pane>

        {/* 7th Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="81%"
              marginRight={10}>
              <TextInput
                name="streetpres"
                value={this.state.fields.presentAddress.address}
                placeholder="Present Address (House / Street / Barangay)"
                width="100%"
                required
                validationMessage="This field is required"
              //    onChange={this.fieldOnChange}
              >
              </TextInput>
            </Pane>
          </Pane>
        </Pane>


        {/* 8th Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="20%"
              marginRight={10}>
              <SelectField
                disabled
                name="provincepres"
                value={this.state.fields.presentAddress.province}
                width="100%"
                // onChange={this.fieldProvinceChange.bind(this)}
                description="Province"
              >
                {this.state.provinceList.data.map((vprovince) => {
                  return <option value={vprovince.code}> {vprovince.name} </option>
                })}
              </SelectField>

            </Pane>
            <Pane width="20%"
              marginRight={10}>
              <SelectField
                name="citypres"
                disabled
                value={this.state.fields.presentAddress.city}
                width="100%"
                description="City/Municipality"
              >
                {this.state.municipalityTempList.data.map((vmuni) => {
                  return <option value={vmuni.code}> {vmuni.name} </option>
                })}
              </SelectField>
            </Pane>


            <Pane width="20%"
              marginRight={10}>
              <SelectField
                disabled
                name="countrypres"
                value={this.state.fields.presentAddress.country}
                width="100%"
                description="Country"
              >
                {this.state.countriesList.data.map((vcountries) => {
                  return <option value={vcountries.alpha2} > {vcountries.name} </option>
                })}
              </SelectField>
            </Pane>
            <Pane width="18%"
              marginRight={10}>
              <Text size={301} marginLeft={1} >Zip Code</Text>
              <TextInputField
                name="zipcodepres"
                value={this.state.fields.presentAddress.zipCode}
                placeholder="Zip Code"
                width="100%"
              >
              </TextInputField>
            </Pane>
          </Pane>
        </Pane>

        {/* 9th Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="50%"
              marginRight={10}>
              <Text marginRight={15}>Permanent address:</Text>
            </Pane>
          </Pane>
        </Pane>

        {/* 10th Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="81%"
              marginRight={10}>
              <TextInput
                name="streetperm"
                value={this.state.fields.permanentAddress.address}
                placeholder="Permanent Address (House / Street / Barangay)"
                width="100%"
              >
              </TextInput>
            </Pane>
          </Pane>
        </Pane>


        {/* 11th Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="20%"
              marginRight={10}>
              <SelectField
                disabled
                name="provinceperm"
                value={this.state.fields.permanentAddress.province}
                width="100%"
                description="Province"
              >
                {this.state.provinceList.data.map((vprovince) => {
                  return <option value={vprovince.code}> {vprovince.name} </option>
                })}
              </SelectField>

            </Pane>
            <Pane width="20%" marginRight={10}>
              <SelectField
                disabled
                name="cityperm"
                value={this.state.fields.permanentAddress.city}
                width="100%"
                description="City/Municipality"
              >
                {this.state.municipalityTemp2List.data.map((vmuni2) => {
                  return <option value={vmuni2.code}> {vmuni2.name} </option>
                })}
              </SelectField>
            </Pane>

            <Pane width="20%"
              marginRight={10}>
              <SelectField
                disabled
                name="countryperm"
                value={this.state.fields.permanentAddress.country}
                width="100%"
                description="Country"
              >
                {this.state.countriesList.data.map((vcountries) => {
                  return <option value={vcountries.alpha2} > {vcountries.name} </option>
                })}
              </SelectField>
            </Pane>
            <Pane width="18%"
              marginRight={10}>
              <Text size={301} marginLeft={1} >Zip Code</Text>
              <TextInput
                name="zipcodeperm"
                value={this.state.fields.permanentAddress.zipCode}
                placeholder="Zip Code"
                width="100%"
              >
              </TextInput>
            </Pane>
          </Pane>
        </Pane>

        {/* 9th Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="100%"
              marginRight={10}>
              <Text marginRight={15}>Preferred mailing address:</Text>
            </Pane>
          </Pane>
        </Pane>

        {this.renderMailAddress()}

        {/* 12th Line */}
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="100%" >
            <Heading size={600} marginLeft={12} >Financial  Information</Heading>
          </Pane>
        </Pane>

        {/* 13rd Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="50%"
              marginRight={10}>
              <SelectField
                disabled
                name="sourceincome"
                value={this.state.fields.incomeSource}
                width="100%"
                description="Source of Income"
              >  <option value=""></option>
                {this.state.incomesourceList.data.map((vincomesource) => {
                  return <option value={vincomesource.code}> {vincomesource.description} </option>
                })}
              </SelectField>
            </Pane>

            <Pane width="50%"
              marginRight={10}>
              <SelectField
                disabled
                name="naturework"
                value={this.state.fields.profession}
                width="100%"
                onChange={this.fieldOnChange}
                description="Nature of Work"
              >
                <option value=""></option>
                {this.state.professionList.data.map((vprofession) => {
                  return <option value={vprofession.code}> {vprofession.description} </option>
                })}
              </SelectField>
            </Pane>
          </Pane>
        </Pane>

        {/* 15th Line */}
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="100%" >
            <Heading size={600} marginLeft={12} >Identification Documents</Heading>
          </Pane>
        </Pane>

        {/* 15th Line */}
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="10%" marginLeft={12} >
            <Text marginRight={10}>Valid ID : </Text>
          </Pane>
          <Pane display="flex" width="30%" >
            <SelectField
              name="vDocType"
              value={this.state.fields.vDocType}
              width="100%"
              onChange={this.ChangeIDType}
            ><option value=""></option>
              {this.state.listDocType.map((x) => {
                return <option value={x.value + x.label}> {x.label} </option>
              })}
            </SelectField>
          </Pane>
          <Pane display="flex" width="10%" marginLeft={12}>
            <ButtonModal parentCallback={this.callbackFunction} height={10} docType={this.state.vDocType.substring(0,4)} imgType={this.state.vDocType.substring(4,30)} />
          </Pane>
          <Pane display="flex" width="20%" marginLeft={12}>
            <Text marginRight={15}>Client's Photo : </Text>
          </Pane>
          <Pane display="flex" width="40%" marginLeft={12} >
            <ButtonModal parentCallback={this.callbackFunction} height={10} docType="0059" imgType="clientPhoto" />
          </Pane>
        </Pane>

        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="10%" ></Pane>
          <Pane display="flex" width="40%" >
            <Table height="100" width="80%" id="IDs" >
                {this.renderTableData()}
            </Table>
          </Pane>
          <Pane display="flex" width="40%" marginLeft={20}>
            <Grid item xs={12} sm={6}><CardMedia component="img" image={this.state.trimmedDataURLClientPhoto} />  </Grid>
          </Pane>
          <Pane display="flex" width="10%" ></Pane>
        </Pane>



        {/*         <Grid item xs={12} sm={6}>{trimmedDataURL? <CardMedia  component="img" image={trimmedDataURL}/> : null} </Grid>     */}







        {/* 16rd Line */}
        {/*
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12} >
            <Pane width="50%" marginRight={10} padding={2}>
              <Pane width="90%" marginRight={10}>
                <SelectField
                  name="vDocType"
                  value={this.state.fields.vDocType}
                  width="100%"
                  onChange={this.fieldOnChange}
                  description="Valid ID"
                >
                  {this.state.listDocType.map((x) => {
                    return <option value={x.value}> {x.label} </option>
                  })}
                </SelectField>
                <ButtonModal docType={this.state.vDocType} imgType="clientID" onChange={this.renderImg.bind()} />
              </Pane>
            </Pane>

            <Pane width="50%" marginRight={10} padding={2}>
              <Pane width="50%" marginRight={10}>
                {this.state.vclientID ? <CardMedia component="img" image={this.state.vclientID} /> : null}
              </Pane>
            </Pane>

            <Pane width="40%" marginRight={10}>
              <p> Client's Photo </p>
              <ButtonModal docType="0059" imgType="clientPhoto" />
            </Pane>

            <Pane width="40%" marginRight={10}>
              <Pane width="50%" marginRight={10}>
                {this.state.vclientPhoto ? <CardMedia component="img" image={this.state.vclientPhoto} /> : null}
              </Pane>
            </Pane>
          </Pane>
        </Pane>

        {/* 16rd Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
          </Pane>
        </Pane>

        {/* 14th Line */}
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="5%" >
          </Pane>
          <Pane display="flex" width="80%" >
            <Heading size={600} marginLeft={12} >Client's Signature</Heading>
          </Pane>
          <Pane display="flex" width="10%" alignItems="right" justifyContent="right">
            <button onClick={this.handleSave.bind(this)} className="btn btn-outline-primary">Clear</button>
          </Pane>
          <Pane display="flex" width="5%" >
          </Pane>
        </Pane>


        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="5%" >
          </Pane>
          <Pane display="flex" width="90%" background="white" borderRadius={3} elevation={1}>
            <SignaturePad options={{ background: "white", minWidth: 5, maxWidth: 5, penColor: 'rgb(0,0,0)' }} ref={ref => this.signaturePad = ref} />
          </Pane>
          <Pane display="flex" width="5%" >
          </Pane>
        </Pane>






        {/* 16th Line */}
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} >

        </Pane>

        {/* 16th Line */}
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="80%" alignItems="center" justifyContent="center">
            <Button height={36} marginRight={16} appearance="primary" intent="danger" onClick={this.uploadImage}>Confirm</Button> {this.state.vloading ? <Spinner /> : ""}
            <Link to={{ pathname: "/" }}>
              <Button height={36} marginRight={16} appearance="primary" intent="danger" >Cancel</Button> </Link>
          </Pane>
        </Pane>
      </div>)
  }
}




















































































































































































































