﻿import React, { Component } from 'react';
import { render } from 'react-dom';
import axios from 'axios';
import './Step.css';
import Panel from 'react-bootstrap/lib/Panel';
import Button from 'react-bootstrap/lib/Button';
import testImg from './Capture.JPG';
import { Tabs, Tab } from 'react-bootstrap-tabs';
import 'bootstrap/dist/css/bootstrap.min.css';
import { InputGroup, InputGroupButton, Glyphicon, FormControl } from 'react-bootstrap';
import moment from 'moment';
import { FormErrors } from './FormErrors';
import Draggable, { DraggableCore } from 'react-draggable';
import './modal.css';
import ItemModal from './ItemModal';

export class NACQueue extends Component {
    displayName = NACQueue.name

    constructor(props) {
        super(props)
        this.state = {
            data: {}, clientInfo: {}, iResult: {}, permAdd: {}, presentAdd: {},
            acResult: [], dedupDesc: [], icdbDesc: [], carsDesc: [],
            acImages: {},
            email: '',
            name: '',
            phone: '',
            dob: '',
            gender: 'M',
            spouseName: 'NA',
            civilStatus: 'Single',
            formErrors: { email: '', name: '', phone: '' },
            emailValid: false,
            nameValid: false,
            phoneValid: false,
            dobValid: false,
            formValid: false,
            isError: false,
            isModalOpen: false,
            items: [
                { id: 0, desc: 'No Data', showModal: false }
            ],
            vRefNo: "01201904150000111"
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.handleItemChange = this.handleItemChange.bind(this)

        /*         this.state = {
                    items: [
                        { id: 0, desc: 'No Data', showModal: false }
                    ],
                } */
    }


    // aka page load
    componentDidMount() {
        this.getCustomerData(); //to get cust data from api

        let newDate = new Date();
        newDate.setDate(newDate.getDate());
        this.setState({ dob: newDate.toISOString().substr(0, 10) });
    }
    
    
    componentDidUpdate(prevProps, prevState) {
        if (prevState.clientInfo !== this.state.clientInfo) {
            if (!this.state.iResult) {
                this.getDedup("0");
            } else {
                this.getDedup(this.state.iResult);
            }

            this.getDocument();

        }
    }

    handleFormUpdate() {
        return e => {
            const field = e.target.name
            const { form } = this.state
            form[field] = e.target.value
            this.setState({ form })
        }
    }

    handleModalHide() {
        return () => {
            let { items } = this.state
            items = items.map(item => ({
                ...item,
                showModal: false,
            }))
            this.setState({ items })
        }
    }

    handleModalShow() {
        return e => {
            e.preventDefault();

            this.setState({ showModal: true });
        };
    }

    handleEditItem(selectedItem) {
        return e => {
            e.preventDefault()
            let { items } = selectedItem// this.state
            items = selectedItem.map(item => ({
                ...item,
                showModal: selectedItem.id === item.id,
            }));
            this.setState({ items });
        };
    }

    handleItemChange(itemId) {
        return e => {
            let { items } = this.state;
            items = items.map(item => {
                if (item.id === itemId) {
                    item[e.target.name] = e.target.value
                }
                return item
            })
            this.setState({ items })
        }
    }

    handleUserInput = (e) => {
        const name = e.target.name;
        const value = e.target.value; 
        this.setState({ [name]: value },
            () => { this.validateField(name, value) }); 
    }

    onSubmit(e) {
        e.preventDefault();

        var form_data = {
            name: this.state.name,
            email: this.state.email,
            phone: this.state.phone,
            dob: this.state.dob
        };
        console.log(JSON.stringify(form_data));
        alert(JSON.stringify(form_data));
        this.setState({ name: '', email: '', phone: '', dob: new Date() });

    }

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let nameValid = this.state.nameValid;
        let phoneValid = this.state.phoneValid;
        let dobValid = this.state.dobValid;

        switch (fieldName) {
            case 'email':
                emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                fieldValidationErrors.email = emailValid ? '' : ' is invalid';
                break;

            case 'phone':
                phoneValid = value.match(/^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/);
                fieldValidationErrors.phone = phoneValid ? '' : ' number is invalid';
                break;

            case 'dob':
                var optimizedBirthday = value.replace(/-/g, "/");
                var myBirthday = new Date(optimizedBirthday);
                var currentDate = new Date().toJSON().slice(0, 10) + ' 01:00:00';
                var myAge = ~~((Date.now(currentDate) - myBirthday) / (31557600000));
                dobValid = myAge > 18;
                fieldValidationErrors.dob = dobValid ? '' : ': you must need to be 18';
                break;

            case 'name':
                nameValid = value.length >= 4;
                fieldValidationErrors.name = nameValid ? '' : ' is too short';
                break;
            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            emailValid: emailValid,
            nameValid: nameValid,
            phoneValid: phoneValid,
            dobValid: dobValid
        }, this.validateForm);
    }

    validateForm() {
        this.setState({ formValid: this.state.emailValid && this.state.nameValid && this.state.phoneValid && this.state.dobValid });
    }

    errorClass(error) {
        return (error.length === 0 ? '' : 'has error');
    }

    authorizationHeader() {
        if (!this.props.keycloak) return {};
        return {
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Authorization": "Bearer " + this.props.keycloak.token
            }
        };
    }

    getCustomerData = () => {
        let vappno = this.state.vRefNo;
        // axios.get('api/WebApi/Getnxt', this.authorizationHeader())
        axios.get(`api/WebApi/${vappno}`)
            //axios.get('api/WebApi/Getnxt', { params: { id: '01201904150000111' } })
            .then(response => {

                let results = null;

                if (typeof (response.data) === "string") {
                    results = JSON.parse(response.data);
                } else {
                    results = response.data;
                }
                let x = results.data.accountNumber;
                this.setState({
                    data: results.data, clientInfo: results.data.clientInfo, application: results.data.application,
                    iResult: results.data.iResult, permAdd: results.data.clientInfo.permanentAddress, presentAdd: results.data.clientInfo.presentAddress, isLoading: false
                });

                return axios.get(x);
            })
            .catch((ex) => {
                console.error(ex);
                this.setState({ isError: true });
            });
    }


    //Function to Load the customer docs and images.
    getDocument = () => {
        let vappno = this.state.vRefNo;
        // axios.get('api/WebApi/Getnxt', this.authorizationHeader())
        axios.get(`api/Documents/${vappno}`)
            //axios.get('api/WebApi/Getnxt', { params: { id: '01201904150000111' } })
            .then(response => {

                let results = null;

                if (typeof (response.data) === "string") {
                    results = JSON.parse(response.data);
                } else {
                    results = response.data;
                }
                let x = results.data.accountNumber;
                this.setState({
                    acImages: results.data
                });

                return axios.get(x);
            })
            .catch((ex) => {
                console.error(ex);
                this.setState({ isError: true });
            });
    }

    //Function to Load the customerdetails data from json.
    getDedup(iCode) {
        console.log(iCode);
        let dedupResults = [];
        let icdbResults = [];
        let carsResults = [];
        axios.get('json/acceptancecriteria.json')
            .then(response => {
                let results1 = null;

                if (typeof (response) === "string") {
                    results1 = JSON.parse(response.data);
                } else {
                    results1 = response.data;
                }
                //this.setState({acResult: results1.data.dedup});

                if (!iCode.dedupResult) {
                    dedupResults.push(results1.dedup.filter(item => item.code === "0"));
                } else {
                    dedupResults.push(results1.dedup.filter(item => item.code === iCode.dedupResult));
                }

                if (!iCode.icdbResult) {
                    icdbResults.push(results1.icdb.filter(item => item.code === "0"));
                } else {
                    icdbResults.push(results1.icdb.filter(item => item.code === iCode.icdbResult));
                }

                if (!iCode.cars.updated.riskRating) {
                    carsResults.push(results1.cars.filter(item => item.code === "0"));
                } else {
                    carsResults.push(results1.cars.filter(item => item.code === iCode.cars.updated.riskRating));
                };

                this.setState({ dedupDesc: dedupResults });
                this.setState({ icdbDesc: icdbResults });
                this.setState({ carsDesc: carsResults });


            })
            .catch((ex) => {
                console.log('dedupError: ' + ex);
            });
    }



    render() {

        //if (this.state.isError)
        //    return(<p>No Data Found.</p>)
        console.log(this.state)
        if (!this.state.data.accountNumber)
            return (<p>Loading data</p>)

        const { clientInfo, application, iResult, permAdd, presentAdd, dedupDesc } = this.state;
        let wname = clientInfo.firstName + " " + clientInfo.middleName + " " + clientInfo.lastName;
        let displayDedup = "No Data";

        if (!this.state.dedupDesc) {
            let displayDedup = this.state.dedupDesc.map(item => { item[0].desc })
        } else {
            console.log(this.state.dedupDesc)
            let displayDedup = "No Data";
        }

        return (

            <div className="addmargin">

                <form onSubmit={this.onSubmit} id="login-form" autoComplete="off">
                    <div className="col-sm-3">
                        {
                            <Panel bsStyle="info" className="blue-border">

                                <Panel.Body className="blue-border">
                                    <p> <img src={testImg} className="image-style"></img></p>
                                    <p> <label className="input-label">Appl. Ref.No.: </label> <input type="text" placeholder="Application No" className="input" name="refno" defaultValue={application.applicationNumber} /> </p>
                                    <p> <label className="input-label">Customer Type: </label> <input type="text" placeholder="Customer Type" className="input" name="custType" defaultValue="Individual" /> </p>
                                    <p> <label className="input-label">Name: </label> <input type="text" placeholder="Name" className="input" name="name" defaultValue={wname} onChange={this.handleUserInput} onItemChange={this.handleUserInput}/> </p>
                                    <p> <label className="input-label">Birthdate: </label><input type="date" className="input" required data-provide="datepicker" name="dob" id="dob" value={this.state.dob} onChange={this.handleUserInput} onItemChange={this.handleUserInput} />
                                    </p>
                                    <p> <label className="input-label">BirthPlace: </label> <input type="text" placeholder="Name" className="input" name="pob" defaultValue={clientInfo.placeOfBirth} /> </p>

                                </Panel.Body>
                            </Panel>
                        }
                    </div>
                    <div className="col-md-9">
                        <section>
                            <Tabs className="tab-container-with-green-border" headerClass="tab-header-bold" activeHeaderClass="tab-header-blue" contentClass="tab-content-yellow">
                                <Tab label="Customer Information" >
                                    <pre>
                                        <p> <label className="input-header">Acceptance Criteria: </label> </p>
                                        {this.state.icdbDesc.map(item =>
                                            <p> <label className="input-label">ICDB Result : </label>  <button onClick={this.handleEditItem(item)} className="input" name="icdbRes" >{item[0].desc}</button>
                                            </p>
                                        )}
                                        {this.state.carsDesc.map(item =>
                                            <p> <label className="input-label">CARS Rating : </label> <input type="text" className="input" name="carsRes" value={item[0].desc} /> </p>
                                        )}
                                        {this.state.dedupDesc.map(item =>
                                            <p> <label className="input-label">DEDUP Result : </label> <input type="text" className="input" name="dedupRes" value={item[0].desc} /> </p>
                                        )}

                                        <br />
                                        <p> <label className="input-header">Other Personal Information: </label> </p>
                                        <p> <label className="input-label">Nationality : </label> <input type="text" placeholder="Nationality" className="sh-input" name="nationality" defaultValue={clientInfo.nationality} /> </p>
                                        <p> <label className="input-label">Gender : </label> <input type="text" placeholder="Gender" className="sh-input" name="gender" defaultValue={this.state.gender} /> </p>
                                        <p> <label className="input-label">Civil Status : </label> <input type="text" placeholder="Civil Status" className="input" name="civilStatus" defaultValue={this.state.civilStatus} /> </p>
                                        <p> <label className="input-label">Name of Spouse : </label> <input type="text" placeholder="Name of Spouse" className="input" name="civilStatus" defaultValue={this.state.spouseName} /> </p>

                                        <br />

                                        <p> <label className="input-header">Contact Information: </label> </p>
                                        <p> <label className="input-label">Mobile Number : </label> <input type="text" placeholder="Mobile Number" className="input" name="phone" defaultValue={clientInfo.mobileNumber} onChange={this.handleUserInput} /> </p>
                                        <p> <label className="input-label">Email Address : </label> <input type="text" placeholder="Email Address" className="input" name="email" defaultValue={clientInfo.email} onChange={this.handleUserInput}/> </p>

                                        <br />

                                        <p> <label className="input-header">Address: </label> </p>
                                        <p> <label className="input-label">Present Address : </label>
                                            <input type="text" placeholder="Unit" className="sh-input" name="unit1" defaultValue={presentAdd.unitNo} />
                                            <input type="text" placeholder="Bldg" className="sh-input" name="bldg1" defaultValue={presentAdd.bldg} />
                                            <input type="text" placeholder="Street" className="sh-input" name="st1" defaultValue={presentAdd.street} />
                                            <input type="text" placeholder="Brgy" className="sh-input" name="brgy1" defaultValue={presentAdd.brgy} />
                                            <input type="text" placeholder="City" className="sh-input" name="city1" defaultValue={presentAdd.city} />
                                            <input type="text" placeholder="ZipCode" className="sh-input" name="zipCode1" defaultValue={presentAdd.zipCode} /> </p>
                                        <p> <label className="input-label">Permanent Address : </label>
                                            <input type="text" placeholder="Unit" className="sh-input" name="unit1" defaultValue={permAdd.unitNo} />
                                            <input type="text" placeholder="Bldg" className="sh-input" name="bldg1" defaultValue={permAdd.bldg} />
                                            <input type="text" placeholder="Street" className="sh-input" name="st1" defaultValue={permAdd.street} />
                                            <input type="text" placeholder="Brgy" className="sh-input" name="brgy1" defaultValue={permAdd.brgy} />
                                            <input type="text" placeholder="City" className="sh-input" name="city1" defaultValue={permAdd.city} />
                                            <input type="text" placeholder="ZipCode" className="sh-input" name="zipCode1" defaultValue={permAdd.zipCode} />  </p>

                                    </pre>
                                </Tab>
                                <Tab label="Documents & Signature">
                                    <pre>.tab-header-bold {"{"}<br />
                                        &nbsp;&nbsp;font-weight: bold;<br />
                                        {"}"}<br />
                                        .nav-tabs a.tab-header-blue.active {"{"}<br />
                                        &nbsp;&nbsp;color: blue;<br />
                                        {"}"}<br /><br />
                                        &lt;Tabs headerClass="tab-header-bold" activeHeaderClass="tab-header-blue"&gt;</pre>
                                </Tab>
                                <Tab label="Account Information" className="tab-content-green">
                                    <pre>&lt;Tabs contentClass="..."&gt;<br />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&lt;Tab className="..." /&gt;<br />
                                        &lt;/Tabs&gt;</pre>
                                </Tab>
                            </Tabs>
                        </section>


                        <div className="panel-default">
                            <button bsStyle="info" type="submit" className="btn btn-primary"> Submit </button>
                        </div>
                    </div>

                    {this.state.items.map((item, index) => (
                        <ItemModal
                            key={item.code}
                            show={item.showModal}
                            onHide={this.handleModalHide()}
                            onItemChange={this.handleItemChange}
                            item={item}
                        />

                    ),
                        console.log(this.state.items))}
                    <hr />
                </form>
            </div>

        );

    }
}


