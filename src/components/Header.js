import React, { Component } from 'react';
import psblogo from './PSBLogo.png';
import './Layout.css';


export class Header extends Component {
  render() {
    return (
      <div>
      <header className="App-header">
      <img src={psblogo} alt="logo" />
      <h1 className="App-title">Central Ops</h1>
    </header>
      </div>
    );
  }
}