import React, { Component } from 'react';
import Panel from 'react-bootstrap/lib/Panel'
import axios from 'axios';
import { Pane, Heading, Text, RadioGroup, Button, Spinner } from 'evergreen-ui';
import { Link } from 'react-router-dom';
import 'antd/dist/antd.css';
import { Radio, DatePicker } from 'antd';
import moment from 'moment';
import { myConfig } from '../config.js';
import { EILSEQ } from 'constants';
import Paragraph from 'antd/lib/skeleton/Paragraph';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import styles from './styles.css';

import SignaturePad from 'react-signature-pad-wrapper';
import Grid from '@material-ui/core/Grid';
import CardMedia from '@material-ui/core/CardMedia';

import Links from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import CameraModal from './CameraModal';
import ButtonModal from './Clientmodal.js';
import Draggable, { DraggableCore } from 'react-draggable';
//import Swiper from 'react-id-swiper';
//import 'react-id-swiper/lib/styles/css/swiper.css';


import './App.css';

//import { Pane, Text, Heading, Checkbox, TextInput, Select, SelectField, RadioGroup, Combobox, Button, Switch,TextInputField } from 'evergreen-ui';
//import TextField from './MUITextfield.js';


//This Component is a child Component of Customers Component
export class ClientConfirmation1 extends Component {

  displayName = ClientConfirmation1.name

  constructor(props) {
    super(props);
    // this.loadProduct = this.loadProduct.bind(this)
    this.state = {
      vStep1: 0,
      vCategoryType: "0",
      fields:
        {
          appLevel: 3,
          appSource: 3,
          birthDate: "",
          birthPlace: "",
          civilStatus: "",
          emailAddress: "",
          fatca: "",
          firstName: "",
          incomeSource: "",
          lastName: "",
          mailingAddress: {},
          middleName: "",
          mobileNumber: "",
          nationality: "",
          permanentAddress: { address: "", city: "", province: "", country: "" },
          presentAddress: {},
          productType: "",
          profession: "",
          referenceNumber: "01201911040000692",
          requestor: "CLIENT",
          responsibilityCode: "",
          spouseName: "NOT APPLICABLE",
          statusCode: "",
          statusDate: "",
          statusDescription: "",
          type: ""
        },
      //this.props.location.state,
      preselectedproductcolor: "",
      convertedSignatureImg: 'x',

    }
    // this.fieldProvinceChange = this.fieldProvinceChange.bind(this)    
  }




  componentDidMount() {
    this.setState({ SuccesfulSaving: true })
    this.setState({ lovs: JSON.parse(sessionStorage.getItem("storageLOVs")) }, () => { console.log(this.state.lovs); });
    if (!this.props.location.state) {
      alert("No Reference Number.");
      this.props.history.push("/")
    } else {
      this.setState({ referenceNumber: this.props.location.state });

      //getting the customer records
      try { this.getCustomerDetails(); }
      catch (e) { this.PopTheMessage("SAVINGS1") } { this.state.PopUpState };
      //getting the customer records
    }


  }


  handleClear() {
    this.signaturePad.instance.clear();
  }


  SelectProduct(newvalue) {
    //let newvalue = x.target.value;
    let state = this.state;
    console.log(newvalue);
    this.setState({
      ...state,
      fields: { ...state.fields, "producttype": newvalue }
    }, () => { console.log(this.state); });
  };

  SubmitApplication() {
    const config = {
      mode: "no-cors",
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/x-www-form-urlencoded",
        "Accept": "application/json"
      }
    };

    const form1 = {
      id: this.state.fields.userid,
      lastName: this.state.fields.lastname,
      gender: this.state.fields.gender,
      documentType: this.state.fields.documenttype,
      mobileNumber: this.state.fields.mobilenumber,
      referenceNumber: this.state.fields.applicationrefno,
      civilStatus: "M",
      type: "I",
      presentAddress: {
        zipCode: this.state.fields.zipcodepres,
        country: this.state.fields.countrypres,
        province: this.state.fields.provincepres,
        city: this.state.fields.citypres,
        address: this.state.fields.streetpres
      },
      permanentAddress: {
        zipCode: this.state.fields.zipcodeperm,
        country: this.state.fields.countryperm,
        province: this.state.fields.provinceperm,
        city: this.state.fields.cityperm,
        address: this.state.fields.streetperm
      },
      mailingAddress: {
        zipCode: this.state.fields.zipcodemail,
        country: this.state.fields.countrymail,
        province: this.state.fields.provincemail,
        city: this.state.fields.citymail,
        address: this.state.fields.streetmail
      },
      incomeSource: this.state.fields.sourceincome,
      emailAddress: this.state.fields.email,
      productType: this.state.fields.producttype,
      monthlyIncome: this.state.fields.monthlyincome,
      birthPlace: this.state.fields.placeofbirth,
      profession: this.state.fields.naturework,
      requestor: "CLIENT",
      birthDate: moment(this.state.fields.birthdate).format('MM/DD/YYYY'),
      firstName: this.state.fields.firstname,
      nationality: this.state.fields.nationality,
      responsibilityCode: this.state.fields.rccode,
      middleName: this.state.fields.middlename,
      spouseName: this.state.fields.spousename,
      fatca: "NO",
      accountFacility: this.state.fields.accountfacilities,
      appLevel: 3,
      appSource: 3,
      processType: 0,
      statusCode: "00",
      statusDate: moment().format("MM/DD/YYYY"),


      customerDetails: JSON.stringify({
        customerNumber: "",
        customerType: "I",
        customerCategory: "IND",
        customerDosri: "N",
        branchCode: this.state.fields.rccode,
        amlRequired: "Y",
        debtorcategory: "",
        fullName: this.state.fields.firstname + " " + this.state.fields.middlename + " " + this.state.fields.lastname,
        lastName: this.state.fields.lastname,
        middleName: this.state.fields.middlename,
        firstName: this.state.fields.firstname,
        shortName: this.state.fields.firstname.substring(0, 1) + this.state.fields.middlename.substring(0, 1) + this.state.fields.lastname.substring(0, 1),
        gender: "M",
        civilStatus: "S",
        birthDate: moment(this.state.fields.birthdate).format("MM/DD/YYYY"),
        birthPlace: this.state.fields.placeofbirth,
        birthCountry: "PH",
        guardian: "NA",
        minor: "N",
        language: "ENG",
        permanentAddress: {
          address: this.state.fields.streetperm,
          barangay: "",
          province: this.state.fields.provinceperm, //TOCHANGE
          city: this.state.fields.cityperm,//TOCHANGE
          country: this.state.fields.countryperm,//TOCHANGE
          zipCode: this.state.fields.zipcodeperm,//TOCHANGE
          knownAs: "",
          motherMaidenName: "",
          educationalStatus: "",
          spouse: {
            name: "",
            employmentStatus: ""
          },
          dependentChildren: "",
          dependentOthers: "",
          accomodation: "O"
        },
        contact: {
          mobileNumber: this.state.fields.mobilenumber,
          mobileCountryCode: "63",
          emailAddress: this.state.fields.email,
          communicationMode: "B",
          homePhoneNumber: this.state.fields.mobilenumber,
          homeCountryCode: "63",
          faxPhoneNumber: "",
          faxCountryCode: ""
        },
        employment: {
          incomeSource: this.state.fields.naturework,//TOCHANGE
          incomeSourceName: this.state.fields.sourceincome,//TOCHANGE
          companyName: "",
          tin: "",
          sssorgsis: "",
          salary: "",
          employmentStatus: "G",
          employmentTenure: "45",
          retirementAge: "0",
          previousDesignation: "",
          previousEmployer: "",
          currentDesignation: "",
          employer: {
            address: "",
            barangay: "",
            city: "",
            telephoneNumber: "",
            phoneNumber: "",
            emailAddress: "",
            countryCode: ""
          }
        },

        mailing: {
          name: this.state.fields.firstname + " " + this.state.fields.middlename + " " + this.state.fields.lastname,
          address: {
            address: this.state.fields.streetmail,
            barangay: "",
            city: this.state.fields.citymail,
            province: this.state.fields.provincemail,
            country: this.state.fields.countrymail,
            zipCode: this.state.fields.zipcodemail
          },
          nationality: "PH"
        },
        risk: {
          level: "A",
          profile: "",
          category: ""
        }
      }),
      AccountDetails:
        JSON.stringify({
          branchCode: this.state.fields.rccode,
          customerNumber: "",
          accountNumber: "DUMMY",
          accountClass: this.state.fields.producttype,
          accountName: this.state.fields.firstname + " " + this.state.fields.middlename + " " + this.state.fields.lastname,
          accountType: "S",
          accountClassType: this.state.AccountClassType,
          currency: "PHP",
          prepaidCardPurpose: "",
          prepaidCardRequired: "N",
          location: "MML",
          media: "",
          frozen: "",
          joint: [
            {
              customerNumber: "",
              customerName: "",
              jointHolderType: "",
              startDate: "",
              endDate: ""
            }
          ],
          productList: "A",
          transactionList: "A",
          specialConditionList: "Y",
          specialConditionTransaction: "Y",
          accountProductRestriction: [
            {
              debitTransactionTag: "Y",
              creditTransactionTag: "Y",
              productCode: "",
              description: ""
            }
          ],
          accountTransfer: ""
        })
    };

    //  console.log(this.state.accountClassType);
    console.log(JSON.stringify(form1));

    axios.post(myConfig.apimiddlewareurl, { APIurl: myConfig.api_uri_createaccount, APIdata: form1, APImethod: "POST" })
      //axios.post('https://10.11.27.104:3001/api/webapi/createaccount', form1)
      .then(response => {
        this.setState({ createaccountresponse: response.data });
        this.setState({ submittedjson: form1 });
        if (this.state.createaccountresponse.errors) {
          this.props.history.push({ pathname: '/ClientDetails1', state: this.props.location.state });
          alert(JSON.stringify(this.state.createaccountresponse.errors, null, 4));
        } else {
          let state = this.state;
          this.setState({
            ...state,
            fields: { ...state.fields, "applicationrefno": JSON.stringify(this.state.createaccountresponse.data.referenceNumber, null, 4).replace(/"/g, "") }
          }, () => {
            this.props.history.push({ pathname: '/PageSuccess1', state: this.state.fields })
          });
        }
      })
      .catch((ex) => {
        console.error(ex);
        this.setState({ isError: true });
        alert("There was an error in submitting your application. Kindly report to the bank's officer. Thank you.");
      });


  }//end submit application

  goToClientDetails() {
    let state = this.state;
    this.setState({
      ...state,
      fields: { ...state.fields, "stepnumber": 1 }
    }, () => {
      if (this.state.fields.NTBETB === "NTB") { this.props.history.push({ pathname: '/ClientDetailsNTB1', state: this.state.fields }) }
      else { this.props.history.push({ pathname: '/ClientDetailsETB1', state: this.state.fields }) }
    });
  };

  goToNextStep(newvalue) {
    //let newvalue = x.target.value;
    console.log(newvalue);
    this.setState({ vStep1: newvalue })
  };

  fieldOnChangeThisState = sender => {
    let fieldName = sender.target.name;
    let value = sender.target.value;
    let state = this.state;
    this.setState({ [fieldName]: value })
  }

  ProductCategorySelected(param) {
    console.log(param);
    this.setState({ vCategoryType: param });
  }

  responsive = { 0: { items: 1 }, 0: { items: 5 } }

  PopTheMessage(param) {
    alert("gangnam")
  }


  render() {

    return (<div>
      <Pane display="flex" height="100%" width="100%" className="Stepper-background"  >
        <Pane width="100%" height="100%">
          {this.renderClientDetails()}
        </Pane>
      </Pane>
    </div>);
  }





  renderClientDetails() {
    const { classes } = this.props;



    return (
      <div>
        <Pane display="flex" width="100%" paddingTop={6} className="Stepper-background" >

          <Pane border="wala" width="5%"> {/*left margin*/}
          </Pane>

          <Pane border="wala" width="90%" >  {/*center content*/}
            <Pane border="wala" width="100%" paddingTop={10} display="flex" alignItems="center">
              <Heading size={300} className="ClientDetailsColor">Your preferred deposit account</Heading>
            </Pane>
            <Pane border="wala" width="100%" paddingTop={5} display="flex" marginBottom={20} alignItems="center">
              <Heading size={600} color="#1070CA" >PSBank Regular Savings with ATM</Heading>
            </Pane>


            <Pane border="wala" width="100%" display="flex" justifyContent="center" alignItems="center" >
              <Pane flexDirection="column" border="default" width="100%" height="100%" alignItems="top" justifyContent="center" >
                <Pane flexDirection="column" display="flex" border="wala" width="100%" height="100%" alignItems="center" justifyContent="center" >
                  <CardMedia component="img" image={this.state.trimmedDataURLClientPhoto} />
                </Pane>
                <Pane display="flex" border="wala" width="100%" height="100%" alignItems="center" justifyContent="center" >
                  <ButtonModal parentCallback={this.callbackFunction} height={10} docType="0059" imgType="clientPhoto" />
                </Pane>
              </Pane>
              <Pane border="wala" flexDirection="row" width="75%" className="Stepper-background" alignItems="top" display="flex">
                <Pane flexDirection="row" width="30%" border="wala" >
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} width={300} border="wala"><Heading size={400} width={700} paddingLeft={15} className="ClientDetailsColor">PERSONAL INFORMATION</Heading> </Pane>
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} border="wala"><Heading size={400} paddingLeft={15} className="ClientDetailsColor">Full Name</Heading></Pane>
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} border="wala"><Heading size={400} paddingLeft={15} className="ClientDetailsColor">Birthdate</Heading></Pane>
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} border="wala"><Heading size={400} paddingLeft={15} className="ClientDetailsColor">Birthplace</Heading></Pane>
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} border="wala"><Heading size={400} paddingLeft={15} className="ClientDetailsColor">Contact Number</Heading></Pane>
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} border="wala"><Heading size={400} paddingLeft={15} className="ClientDetailsColor">Email Address</Heading></Pane>
                </Pane>

                <Pane flexDirection="column" width="30%" border="wala" >
                  <Pane flexDirection="row" height={30} border="wala" ><Heading size={400} paddingLeft={5} className="ClientDetailsColor"></Heading></Pane>
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} border="wala"><Heading size={100} paddingLeft={5} className="ClientDetailsColor">{this.state.fields.firstName + " " + this.state.fields.middleName + " " + this.state.fields.lastName}</Heading></Pane>
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} border="wala"><Heading size={100} paddingLeft={5} className="ClientDetailsColor">{this.state.fields.birthDate}</Heading></Pane>
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} border="wala"><Heading size={100} paddingLeft={5} className="ClientDetailsColor">{this.state.fields.birthPlace}</Heading></Pane>
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} border="wala"><Heading size={100} paddingLeft={5} className="ClientDetailsColor">{this.state.fields.mobileNumber}</Heading></Pane>
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} border="wala"><Heading size={100} paddingLeft={5} className="ClientDetailsColor">{this.state.fields.emailAddress}</Heading></Pane>
                </Pane>

                <Pane flexDirection="row" width="15%" border="wala" >
                  <Pane flexDirection="row" height={30} border="wala" ><Heading size={400} paddingLeft={5} className="ClientDetailsColor"></Heading></Pane>
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} border="wala"><Heading size={400} paddingLeft={5} className="ClientDetailsColor">Nationality</Heading></Pane>
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} border="wala"><Heading size={400} paddingLeft={5} className="ClientDetailsColor">US Person</Heading></Pane>
                </Pane>


                <Pane flexDirection="column" width="15%" border="wala" >
                  <Pane flexDirection="row" height={30} border="wala" ><Heading size={400} paddingLeft={5} className="ClientDetailsColor"></Heading></Pane>
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} border="wala"><Heading size={100} paddingLeft={5} className="ClientDetailsColor">{this.state.fields.nationality}</Heading></Pane>
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} border="wala"><Heading size={100} paddingLeft={5} className="ClientDetailsColor">{this.state.fields.fatca}</Heading></Pane>
                </Pane>
              </Pane>
            </Pane>



            <Pane border="wala" width="100%" height={30} display="flex" justifyContent="center" alignItems="center" > </Pane>




            <Pane border="wala" width="100%" display="flex" justifyContent="center" alignItems="center" >
              <Pane border="wala" flexDirection="row" width="100%" className="Stepper-background" alignItems="top" display="flex">
                <Pane flexDirection="row" width="20%" border="wala" >
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} width={300} border="wala"><Heading size={400} width={700} paddingLeft={10} className="ClientDetailsColor">ADDRESS</Heading> </Pane>
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} border="wala"><Heading size={400} paddingLeft={10} className="ClientDetailsColor">Present Address</Heading></Pane>
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} border="wala"><Heading size={400} paddingLeft={10} className="ClientDetailsColor">Mailing Address</Heading></Pane>
                </Pane>

                <Pane flexDirection="column" width="80%" border="wala" >
                  <Pane flexDirection="row" height={30} border="wala" ><Heading size={400} paddingLeft={5} className="ClientDetailsColor"></Heading></Pane>
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} border="wala"><Heading size={100} paddingLeft={10} className="ClientDetailsColor">{this.state.fields.permanentAddress.address+" "+this.state.fields.permanentAddress.city+" "+this.state.fields.permanentAddress.province+" "+this.state.fields.permanentAddress.country}</Heading></Pane>
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} border="wala"><Heading size={100} paddingLeft={10} className="ClientDetailsColor">{this.state.fields.permanentAddress.address+" "+this.state.fields.permanentAddress.city+" "+this.state.fields.permanentAddress.province+" "+this.state.fields.permanentAddress.country}</Heading></Pane>
                </Pane>
              </Pane>
            </Pane>


            <Pane border="wala" width="100%" height={30} display="flex" justifyContent="center" alignItems="center" > </Pane>


            <Pane border="wala" width="100%" display="flex" justifyContent="center" alignItems="center" >
              <Pane border="wala" flexDirection="row" width="100%" className="Stepper-background" alignItems="top" display="flex">
                <Pane flexDirection="row" width="20%" border="wala" >
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} width={300} border="wala"><Heading size={400} width={700} paddingLeft={10} className="ClientDetailsColor">FINANCIAL INFORMATION</Heading> </Pane>
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} border="wala"><Heading size={400} paddingLeft={10} className="ClientDetailsColor">Source of funds</Heading></Pane>
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} border="wala"><Heading size={400} paddingLeft={10} className="ClientDetailsColor">Nature of work</Heading></Pane>
                </Pane>

                <Pane flexDirection="column" width="30%" border="wala" >
                  <Pane flexDirection="row" height={30} border="wala" ><Heading size={400} paddingLeft={5} className="ClientDetailsColor"></Heading></Pane>
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} border="wala"><Heading size={100} paddingLeft={10} className="ClientDetailsColor">{this.state.fields.incomeSource}</Heading></Pane>
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} border="wala"><Heading size={100} paddingLeft={10} className="ClientDetailsColor">{this.state.fields.profession}</Heading></Pane>
                </Pane>

                <Pane flexDirection="row" width="20%" border="wala" >
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} width={300} border="wala"><Heading size={400} width={700} paddingLeft={5} className="ClientDetailsColor">IDENTIFICATION DOCUMENTS</Heading> </Pane>
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} border="wala"><Heading size={400} paddingLeft={10} className="ClientDetailsColor">Valid ID</Heading></Pane>
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} border="wala"><Heading size={400} paddingLeft={10} className="ClientDetailsColor">Other Documents</Heading></Pane>
                </Pane>


                <Pane flexDirection="column" width="30%" border="wala" >
                  <Pane flexDirection="row" height={30} border="wala" ><Heading size={400} paddingLeft={5} className="ClientDetailsColor"></Heading></Pane>
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} border="wala"><Heading size={100} paddingLeft={10} className="ClientDetailsColor">Valid ID</Heading></Pane>
                  <Pane flexDirection="row" display="flex" alignItems="center" height={30} border="wala"><Heading size={100} paddingLeft={10} className="ClientDetailsColor">Other Documents</Heading></Pane>
                </Pane>
              </Pane>
            </Pane>


            <Pane border="wala" width="100%" height={40} display="flex" justifyContent="center" alignItems="center" > </Pane>

            <Pane border="wala" background="tint2" width="100%" height={30} display="flex" alignItems="center" >
              <Pane border="wala" width="90%"> <Heading size={400} paddingLeft={10} className="ClientDetailsColor">SPECIMEN SIGNATURE </Heading>  </Pane>
              <Pane border="wala" justifyContent="right" display="flex"> <button marginRight={2} height={36} onClick={this.handleClear.bind(this)} appearance="default">Clear</button></Pane>
            </Pane>

            <Pane border="wala" elevation={3} width="100%" display="flex" justifyContent="center" alignItems="center" >
              <SignaturePad options={{ background: "white", minWidth: 5, maxWidth: 5, penColor: 'rgb(0,0,0)' }} ref={ref => this.signaturePad = ref} />
            </Pane>



            <Pane width="100%" paddingTop={40} justifyContent="center" >
              <Pane height={60} display="flex" justifyContent="center" alignItems="center" >
                <Button height={36} marginRight={16} appearance="primary" intent="danger" onClick={this.handleSave.bind(this)}>Confirm</Button> {this.state.vloading ? <Spinner /> : ""}
                <Link to={{ pathname: "/" }}>
                  <Button height={36} marginRight={16} appearance="primary" intent="danger" >Cancel</Button> </Link>
              </Pane>
            </Pane>

          </Pane>    {/*end center content*/}

          <Pane border="wala" width="5%">  {/*right margin*/}
          </Pane>

        </Pane >
      </div >
    );



  } //end of render client details




















  handleSave() {
    if (this.signaturePad.isEmpty()) {
      alert('Please provide a signature first.');
    } else {

      //upload signature first
      try {
        let blob = this.dataURItoBlob(this.signaturePad.toDataURL());
        let fileReaderInstance = new FileReader();
        fileReaderInstance.readAsDataURL(blob);
        fileReaderInstance.onload = () => {
          let base64data = fileReaderInstance.result;
          this.setState({ convertedSignatureImg: fileReaderInstance.result.substring(22) }, () => {
            this.uploadImage("0002", this.state.convertedSignatureImg),  //upload signature first
              this.uploadImage("0001", this.state.trimmedDataURLClientPhoto.substring(23)),
              console.log(this.state.convertedSignatureImg)
            this.uploadConfirmation();
            ;
          });
        }
      }
      catch (e) {
        alert("Error encountered in confirming client details")
      }
      //end upload signature first


    }
  }





  uploadImage(ImageTypeToUpload, vImageBlob) {
    console.log(ImageTypeToUpload);
    console.log(vImageBlob);
    const config = {
      mode: "no-cors",
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/x-www-form-urlencoded",
        "Accept": "application/json"
      }
    };
    const formE =
      {
        file: vImageBlob,
        isFront: 1,
        referenceNumber: this.props.location.state,
        level: 3,
        documentType: ImageTypeToUpload,
        requestor: "CLIENT",
        fileExtension: "jpg"
      };
    axios.post(myConfig.apimiddlewareurl, { APIurl: myConfig.apiuri_uploaddocument, APIdata: formE, APImethod: "POST" })
      .then(response => {
        try {
          if (response.request.statusText != "OK") { this.setState({ SuccesfulSaving: false }); }
        }
        catch (e) { this.setState({ SuccesfulSaving: false }); }
      })
      .catch((ex) => {
        console.error(ex);
        this.setState({ SuccesfulSaving: false });
      });
  }//end uplodForm


  callbackFunction = (childData) => {
    console.log(childData.DocumentDescription);
    console.log(childData.DocumentCode);
    if (childData.DocumentCode === "0059") {
      this.setState({
        trimmedDataURLClientPhoto: childData.DocumentImage
      });
    }
    else {
      if (childData.DocumentDescription.trim() != "") {
        this.setState(prevState => ({ CapturedIDList: [...prevState.CapturedIDList, { DocumentCode: childData.DocumentCode, DocumentImage: childData.DocumentImage, DocumentImageFileExt: childData.DocumentImageFileExt, DocumentDescription: childData.DocumentDescription }] }))
      }
    }
  }







  uploadImage2 = (value, event) => {
    this.setState({ vloading: true });
    try {
      this.setState({
        convertedImgClientPhoto: JSON.parse(sessionStorage.getItem("clientPhoto")).DocumentImage.substring(23),
        convertedImgClientID: JSON.parse(sessionStorage.getItem("clientID")).DocumentImage.substring(23),
        trimmedDataURLClientID: sessionStorage.getItem("clientID").toDataURL,
        trimmedDataURLClientPhoto: sessionStorage.getItem("clientPhoto")
      });
      this.setState({ trimmedDataURL: sessionStorage.getItem("ClientSig") })
    }
    catch (e) {
      console.log(e);
    }

    if (this.state.convertedImg != 'x') {
      const config = {
        mode: "no-cors",
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/x-www-form-urlencoded",
          "Accept": "application/json"
        }
      };
      const formD =
        {
          file: this.state.convertedImg,
          isFront: 1,
          referenceNumber: this.props.location.state,
          level: 3,
          documentType: 2,
          requestor: "CLIENT",
          fileExtension: "jpg"
        };
      console.log(formD);

      axios.post(myConfig.apimiddlewareurl, { APIurl: myConfig.apiuri_uploaddocument, APIdata: formD, APImethod: "POST" })
        .then(response => {
          this.setState({ createaccountresponse: response.data });
          this.setState({ submittedjson: formD });
          // console.log(form1);
          if (this.state.createaccountresponse.errors) {
            alert(JSON.stringify(this.state.createaccountresponse.errors, null, 4));
          } else {
            this.setState({ ImageSuccessfullyUploaded: "T" });
            //this.uploadConfirmation();
            // this.uploadImageClientPhoto();
            // this.uploadImageClientID();
            // this.setState({ vloading: false });
          }
        })
        .catch((ex) => {
          console.log(ex);
          this.setState({ isError: true });
          alert(JSON.stringify(this.state.createaccountresponse.errors, null, 4));
        });

    } else { this.uploadConfirmation(); }
    this.setState({ vloading: false });
  }//end uplodForm

  uploadConfirmation = (value, event) => {
    this.setState({ ProductType: { value } });
    const config = {
      mode: "no-cors",
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/x-www-form-urlencoded",
        "Accept": "application/json"
      }
    };

    const form1 =
      {
        dedupDisposition: 0,
        riskProfile: "",
        referenceNumber: this.props.location.state,
        processType: 0,
        riskScore: 0,
        createdBy: "CLIENT",
        riskRating: "",
        remarks: "Client details confirmed by client",
        status: 20,
        statusDate: moment().format("MM/DD/YYYY"), ///date.getmonth()+1 + "/" + date.getDate() + "/" +  date.gettyear(),//Date.now.format("mm/dd/yyyy")"",
        icdbResult: 0,
        appLevel: 3,
        statusDescription: "CLIENT CONFIRMATION"
      };

    axios.post(myConfig.apimiddlewareurl, { APIurl: myConfig.apiuri_statusupdate, APIdata: form1, APImethod: "POST" })
      .then(response => {
        this.setState({ createaccountresponse: response.data });
        this.setState({ submittedjson: form1 });
        console.log(this.state.createaccountresponse.errors);
        if (this.state.createaccountresponse.errors) {
          alert(JSON.stringify(this.state.createaccountresponse.errors, null, 4));
        } else {
          alert("Your personal details has been confirmed. Thank you.");
          this.props.history.push({ pathname: '/', state: this.state.fields });
        }
      })
      .catch((ex) => {
        console.error(ex);
        this.setState({ isError: true });
        alert(JSON.stringify(this.state.createaccountresponse.errors, null, 4));
      });
  }//end uplodForm





  uploadImageClientPhoto = (value, event) => {
    if (this.state.convertedImgClientPhoto != 'x' && this.state.convertedImgClientPhoto != '') {
      const config = {
        mode: "no-cors",
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/x-www-form-urlencoded",
          "Accept": "application/json"
        }
      };

      const formF =
        {
          file: this.state.convertedImgClientPhoto,
          isFront: 1,
          referenceNumber: this.props.location.state,
          level: 3,
          documentType: 1,
          requestor: "CLIENT",
          fileExtension: "jpg"
        };

      axios.post(myConfig.apimiddlewareurl, { APIurl: myConfig.apiuri_uploaddocument, APIdata: formF, APImethod: "POST" })
        .then(response => {
          this.setState({ createaccountresponse: response.data });
          this.setState({ submittedjson: formF });
          console.log(this.state.createaccountresponse.errors);
          if (this.state.createaccountresponse.errors) {
            alert(JSON.stringify(this.state.createaccountresponse.errors, null, 4));
          } else {
            this.setState({ ImageSuccessfullyUploaded: "T" });
          }
        })
        .catch((ex) => {
          console.error(ex);
          this.setState({ isError: true });
          alert(JSON.stringify(this.state.createaccountresponse.errors, null, 4));
        });

    }
  }//end uplodForm

  uploadImageClientID = (value, event) => {
    if (this.state.convertedImgClientID != 'x' && this.state.convertedImgClientID != '') {
      const config = {
        mode: "no-cors",
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/x-www-form-urlencoded",
          "Accept": "application/json"
        }
      };

      const formE =
        {
          file: this.state.convertedImgClientID,
          isFront: 1,
          referenceNumber: this.props.location.state,
          level: 3,
          documentType: this.state.fields.vDocType,
          requestor: "CLIENT",
          fileExtension: "jpg"
        };

      axios.post(myConfig.apimiddlewareurl, { APIurl: myConfig.apiuri_uploaddocument, APIdata: formE, APImethod: "POST" })
        .then(response => {
          this.setState({ createaccountresponse: response.data });
          this.setState({ submittedjson: formE });
          console.log(this.state.createaccountresponse.errors);
          if (this.state.createaccountresponse.errors) {
            alert(JSON.stringify(this.state.createaccountresponse.errors, null, 4));
          } else {
            this.setState({ ImageSuccessfullyUploaded: "T" });
          }
        })
        .catch((ex) => {
          console.error(ex);
          this.setState({ isError: true });
          alert(JSON.stringify(this.state.createaccountresponse.errors, null, 4));
        });

    }
  }//end uplodForm

  getCustomerDetails = async () => {
    const headers = { "headers": "Access-Control-Allow-Origin" }
    let rc = localStorage.getItem("groups");
    let vappno = this.state.vRefNo;
    axios.post(myConfig.apimiddlewareurl, { APIurl: myConfig.apiuri_getcustomerforconfirmation, APIdata: "referenceNumber=" + this.props.location.state, APImethod: "GET" })
      .then(response => {
        let results = null;
        if (typeof (response.data) === "string") {
          results = JSON.parse(response.data);
        } else {
          results = response.data;
        }
        this.setState({ fields: results.data }, () => { console.log(this.state); });
        this.setState({ vloading: false });
      })
      .catch((ex) => {
        console.error(ex);
        this.setState({ isError: true });
        alert("Reference number doesn't exists");
        this.props.history.push({ pathname: '/', state: this.state.fields });
      });

  }; //end getCustomerDetails









  dataURItoBlob(dataURI) {
    let byteString = atob(dataURI.split(',')[1]);
    // separate out the mime component
    let mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
    let ab = new ArrayBuffer(byteString.length);
    let ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    let blob = new Blob([ab], { type: mimeString });
    return blob;
  }








}









