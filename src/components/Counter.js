import React, { Component } from 'react';
import { render } from 'react-dom';
import axios from 'axios';
import Panel from 'react-bootstrap/lib/Panel';
import testImg from './Capture.JPG';
import './Step.css';

export class Counter extends Component {
    displayName = Counter.name

    constructor(props) {
        super(props)
        this.state = {
            clientDocs: [], loading: true,
            selectedDoc: 0
        };
    };

    //Function to Load the customerdetails data from json.
    getDocument = () => {
        axios.get('json/getdocument.json')
            .then(response => {
                let results1 = null;

                if (typeof (response) === "string") {
                    results1 = JSON.parse(response.data);
                } else {
                    results1 = response.data;
                }
                this.setState({ clientDocs: results1.data, loading: false });
                console.log(this.state.clientDocs);
            })
            .catch((ex) => {
                console.log('getDocument: ' + ex);
            });

    };

handleHoverOff(event) {
    this.setState({hover: true})
}

    componentDidMount() {
        this.getDocument();
    }

    render() {

        if (!this.state.clientDocs)
            return (<p>Loading data</p>)
        return (<div className="addmargin">
            <div className="col-md-3" >
                <Panel bsStyle="info" className="centeralign">
                    <Panel.Heading>
                        <Panel.Title componentClass="h3">Client Documents</Panel.Title>
                    </Panel.Heading>
                    <Panel.Body>
                        <ul>
                            {this.state.clientDocs.map(customer => <li className="special" onClick={() => this.setState({ selectedCustomer: customer.frontImage.image })} > <a> {customer.documentType.description} </a>  </li>)}
                        </ul>
                    </Panel.Body>
                </Panel>

            </div>
            <div className="col-md-6 content-center">
                <img src={this.state.selectedCustomer}  style={{flex:1, alignSelf: 'stretch', height: undefined, width: undefined, justifyContent: 'center'}}></img>
            </div>
            <br />
        </div>)

    }
}
