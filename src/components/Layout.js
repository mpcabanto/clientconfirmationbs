import React, { Component } from 'react';
import { NavHeader } from './NavHeader';
import { Header } from './Header';
import psblogo from './PSBLogo.png';
import './App.css';
import Clock from 'react-live-clock';
import header from './Header.jpg';


export class Layout extends Component {
  displayName = Layout.name

  render() {
    return (
      <div width="100%"  >
        <div id="psblogo" height="100%">
          <header >
          <img src={header} height="100%" width="100%"  />
          </header>
        </div>
        <div  className="App-separator">
        <a height="100%" width="100%"   className="App-separator">{"\u00a0"} </a>
        </div>
        <div className="body">
          <section >
            {this.props.children}
          </section>
        </div>
        <div className="footer-wrap2">
          <div className="member">CentralOps © 2019. Philippine Savings Bank.</div>
        </div>
      </div>
    );
  }
}



