import React, { Component } from 'react';
import './Modal/modal.css';

import PropTypes from 'prop-types';
import Draggable, { DraggableCore } from 'react-draggable';
import { Pane, Text, Heading, TextInput, Select, SelectField, RadioGroup, Combobox, Button, Switch, TextInputField } from 'evergreen-ui';
import Camera, { FACING_MODES, IMAGE_TYPES } from './Camera';
import './Camera/reset.css';
import 'react-html5-camera-photo/build/css/index.css';

import ThreeSixtyIcon from '@material-ui/icons/ThreeSixty';
import SaveIcon from '@material-ui/icons/Save';
import GridItem from "./Grid/GridItem.jsx";
import GridContainer from "./Grid/GridContainer.jsx";
import Grid from "@material-ui/core/Grid";
import CardMedia from '@material-ui/core/CardMedia';
import Links from '@material-ui/core/Link';


class ButtonModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imgFile: '',
      isOpen: false,
      idealFacingMode: FACING_MODES.ENVIRONMENT,
      activeDrags: 0,
      convertedImg: '',
      deltaPosition: {
        x: 0, y: 0
      },
      controlledPosition: {
        x: -400, y: 200
      },
      Imagefields: { DocumentCode: "", DocumentImage: "", DocumentImageFileExt: "", DocumentDescription:"" }
    };
    //        this.renderButtons = this.renderButtons.bind(this);



  };


  dataURItoBlob(dataURI) {
    let byteString = atob(dataURI.split(',')[1]);

    // separate out the mime component
    let mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    let ab = new ArrayBuffer(byteString.length);
    let ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    let blob = new Blob([ab], { type: mimeString });
    return blob;
  }

  padWithZeroNumber(number, width) {
    number = number + '';
    return number.length >= width
      ? number
      : new Array(width - number.length + 1).join('0') + number;
  }

  getFileExtention(blobType) {
    // by default the extention is .png
    let extention = IMAGE_TYPES.PNG;

    if (blobType === 'image/jpeg') {
      extention = IMAGE_TYPES.JPG;
    }
    return extention;
  }

  getFileName(imageNumber, blobType) {
    const prefix = 'photo';
    const photoNumber = this.padWithZeroNumber(imageNumber, 4);
    const extention = this.getFileExtention(blobType);

    return `${prefix}-${photoNumber}.${extention}`;
  }


  downloadImageFileFomBlob(blob, imageNumber) {
    window.URL = window.webkitURL || window.URL;

    let anchor = document.createElement('a');
    anchor.download = this.getFileName(imageNumber, blob.type);
    anchor.href = window.URL.createObjectURL(blob);
    let mouseEvent = document.createEvent('MouseEvents');
    mouseEvent.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
    anchor.dispatchEvent(mouseEvent);
  }

  downloadImageFile(dataUri, imageNumber) {
    let blob = this.dataURItoBlob(dataUri);
    this.convertBlob(blob);
    this.setState({ imgFile: window.URL.createObjectURL(blob) })
  }

  convertBlob(blob) {
    let fileReaderInstance = new FileReader();
    fileReaderInstance.readAsDataURL(blob);
    fileReaderInstance.onload = () => {
      let base64data = fileReaderInstance.result;
      this.setState({ convertedImg: base64data }   );
      let state = this.state;
      this.setState({ ...state, Imagefields: { ...state.Imagefields,"DocumentCode":this.props.docType, "DocumentImage":base64data ,"DocumentImageFileExt":"jpg","DocumentDescription":this.props.imgType}} );
    }
  }

  open = () => this.setState({ isOpen: true });

  close = () => this.setState({ isOpen: false }, () => { this.sendData(); });

  onTakePhoto(dataUri) {
    this.downloadImageFile(dataUri, this.imageNumber)  ;
    this.imageNumber += 1;
  }



  onCameraError(error) {
    console.error('onCameraError', error);
  }

  onCameraStart(stream) {
    console.log('onCameraStart');
  }

  onCameraStop() {
    console.log('onCameraStop');
  }


  renderButtons() {
    const { classes } = this.props;

    return (
      <div>
        <Button variant="contained" size="small" color="info" onClick={(e) => {
          if (this.state.idealFacingMode === FACING_MODES.USER) {
            this.setState({ idealFacingMode: FACING_MODES.ENVIRONMENT });
          } else {
            this.setState({ idealFacingMode: FACING_MODES.USER });
          }
        }} >
          <ThreeSixtyIcon />
          Switch
      </Button>
        <Button variant="contained" color="info" onClick={this.close} >Close</Button>
      </div>
    );
  }

  sendData = () => {
    console.log(this.state.Imagefields.DocumentDescription);
    if (this.state.Imagefields.DocumentDescription !=="")
    {this.props.parentCallback(this.state.Imagefields);}
    let state = this.state;
    this.setState({ ...state, Imagefields: { ...state.Imagefields,"DocumentCode":"", "DocumentImage":"" ,"DocumentImageFileExt":"","DocumentDescription":""}} );
  }

  renderImg() {
    if (this.state.imgFile)
     // console.log(this.state.imgFile)
    //let tempStorage = { DocumentCode: this.props.docType, DocumentImage: this.state.convertedImg, DocumentImageFileExt: "jpg" }
    //sessionStorage.setItem(this.props.imgType, JSON.stringify(tempStorage))
    return (
      <div>
        <CardMedia
          component="img"
          image={this.state.imgFile}
        />
      </div>
    );
  }

  componentDidMount() {
    let state = this.state;
    this.setState({ ...state, Imagefields: { ...state.Imagefields,"DocumentCode":"", "DocumentImage":"" ,"DocumentImageFileExt":"","DocumentDescription":""}} );
  }

  render() {
    console.log(this.state);   
    console.log(this.props);
    console.log(this.props.imgType);
    const dragHandlers = { onStart: this.onStart, onStop: this.onStop };
    const { deltaPosition, controlledPosition } = this.state;

    if (this.state.isOpen) {
      return (
        <div className="basic-modal" onClick={this.close}>
          <Draggable positionOffset={{ x: '-40%', y: 0 }} {...dragHandlers}>
            <div onClick={e => e.stopPropagation()} className="basic-modal-content">

              <GridContainer alignItems="center" >
                <GridItem xs={12} sm={12} md={12}>
                  <Camera
                    onTakePhoto={(dataUri) => { this.onTakePhoto(dataUri); }}
                    onCameraError={(error) => { this.onCameraError(error); }}
                    idealFacingMode={this.state.idealFacingMode}
                    idealResolution={{ width: '50%', height: '50%' }}
                    imageType={IMAGE_TYPES.JPG}
                    imageCompression={0.97}
                    isMaxResolution={false}
                    isImageMirror={false}
                    isSilentMode={false}
                    isDisplayStartCameraError={true}
                    sizeFactor={1}
                    onCameraStart={(stream) => { this.onCameraStart(stream); }}
                    onCameraStop={() => { this.onCameraStop(); }}
                    ref={(cam) => {
                      this.camera = cam;
                    }}
                  />

                  <Grid container alignItems="center">
                    <Grid item xs={12} sm={12} md={4}>
                    </Grid>
                    <Grid item xs={12} sm={12} md={4}>
                      {this.renderButtons()}
                      {this.renderImg()}
                    </Grid>
                    <Grid item xs={12} sm={12} md={4}>
                    </Grid>
                  </Grid>
                </GridItem>
              </GridContainer>
            </div>
          </Draggable>
        </div>
      )
    }
{/* <button onClick={this.open} className="btn btn-outline-primary">Capture</button>    */}
    return (
      <Links onClick={this.open} > Retake </Links>
    );
  }
}

export default ButtonModal;
